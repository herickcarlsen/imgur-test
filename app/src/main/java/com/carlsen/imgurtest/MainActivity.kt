package com.carlsen.imgurtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.carlsen.imgurtest.adapter.ImgurAdapter
import com.carlsen.imgurtest.entity.Image
import com.carlsen.imgurtest.entity.ImgurResponse
import com.carlsen.imgurtest.retrofit.ApiInterface
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private var ingredientAdapter = ImgurAdapter()
    private var arrImg = ArrayList<Image>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestImg()
    }


    private fun requestImg(){

        val apiInterface = ApiInterface.create().getImgur()

        apiInterface.enqueue( object : Callback<ImgurResponse> {
            override fun onResponse(call: Call<ImgurResponse>?, response: Response<ImgurResponse>?) {

                if(response?.body() != null) {
                    Log.d("API-Response: ", response?.body()!!.data.toString())

                    response.body()!!.data.forEach {
                        if(it.images != null)
                            arrImg.add(it.images.first())
                    }
                    ingredientAdapter.setData(arrImg)

                    rv_imgur.layoutManager = GridLayoutManager(this@MainActivity,4)
                    rv_imgur.adapter = ingredientAdapter
                }

            }
            override fun onFailure(call: Call<ImgurResponse>, t: Throwable) {
                Log.d("API-Response: ", "error: $t")
            }
        })

    }
}