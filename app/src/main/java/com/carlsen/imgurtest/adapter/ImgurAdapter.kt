package com.carlsen.imgurtest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.carlsen.imgurtest.R
import com.carlsen.imgurtest.entity.Image
import kotlinx.android.synthetic.main.rv_imgur_item.view.*

class ImgurAdapter : RecyclerView.Adapter<ImgurAdapter.ImgurViewHolder>() {

    var arrImg = ArrayList<Image>()
    private var ctx : Context? = null


    class ImgurViewHolder(view: View): RecyclerView.ViewHolder(view)

    fun setData(arr : List<Image>){
        arrImg = arr as ArrayList<Image>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImgurViewHolder {
        ctx = parent.context
        return ImgurViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rv_imgur_item,parent,false))
    }

    override fun onBindViewHolder(holder: ImgurViewHolder, position: Int) {


        Glide.with(ctx!!).load(arrImg[position].link).into(holder.itemView.imgur_img)


    }

    override fun getItemCount(): Int {
        return arrImg.size
    }
}