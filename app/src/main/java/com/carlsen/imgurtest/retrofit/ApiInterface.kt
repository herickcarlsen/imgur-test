package com.carlsen.imgurtest.retrofit

import com.carlsen.imgurtest.entity.ImgurResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiInterface {

    @Headers("authorization: " + "Client-ID 1ceddedc03a5d71")
    @GET("search/?q=cats")
    fun getImgur() : Call<ImgurResponse>

    companion object {

        var BASE_URL = "https://api.imgur.com/3/gallery/"

        fun create() : ApiInterface{
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)

            val client: OkHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()

            val retrofit =  Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)


        }


    }



}