# Imgur Test

The objective of the application is to fetch cats images from ​https://imgur.com​, and show the images on gallery

# Technologies

-Kotlin

-RecyclerView

-Glide

-Retrofit

This project was develop for study purpose only

## DEMO:

![imgur-test-gif](imgur.gif)
